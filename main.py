import os
import glob as glob
import matplotlib.pyplot as plt
import cv2
import random

# use python3.10
# wget https://learnopencv.s3.us-west-2.amazonaws.com/pothole_dataset.zip
# unzip -q pothole_dataset.zip
# git clone https://github.com/WongKinYiu/yolov7.git
# cd yolov7
# pip install -r requirements.txt
# wget https://github.com/WongKinYiu/yolov7/releases/download/v0.1/yolov7-tiny.pt


# TRAIN 1
# python yolov7/train.py --epochs 100 --workers 4 --batch-size 32 --data pothole.yaml --img 640 640 --cfg yolov7_pathole_tiny.yaml --weights 'yolov7-tiny.pt' --name yolov7_tiny_pathole_fixed_res --hyp hyp.scratch.tiny.yaml
# TEST
# python test.py --weights runs/train/yolov7_tiny_pothole_fixed_res/weights/best.pt --task test --data pothole.yaml

# Tiny Model Multi-Resolution Training
# python train.py --epochs 100 --workers 4 --device 0 --batch-size 32 --data pothole.yaml --img 640 640 --cfg yolov7_pothole-tiny.yaml --weights 'yolov7-tiny.pt' --name yolov7_tiny_pothole_multi_res --hyp hyp.scratch.tiny.yaml --multi-scale
# TEST
# python test.py --weights runs/train/yolov7_tiny_pothole_multi_res/weights/best.pt --task test --data pothole.yaml

class Plotter:

    def yolo2bbox(self, bboxes):
        xmin, ymin = bboxes[0]-bboxes[2]/2, bboxes[1]-bboxes[3]/2
        xmax, ymax = bboxes[0]+bboxes[2]/2, bboxes[1]+bboxes[3]/2
        return xmin, ymin, xmax, ymax

    def denormalize_ccordinates(self, w, h, x1, y1, x2, y2):
        return int(x1*w), int(y1*h), int(x2*w), int(y2*h)

    def get_thickness(self, w):
        return max(2, int(w/275))

    def build_rectangle_opencv(self, w, image, xmin, ymin, xmax, ymax):
        thickness = self.get_thickness(w)
        cv2.rectangle(
            image, 
            (xmin, ymin), (xmax, ymax),
            color=(0, 0, 255),
            thickness=thickness
        )

    def plot_box(self, image, bboxes):
        h, w, _ = image.shape
        
        for _, box in enumerate(bboxes):
            x1, y1, x2, y2 = self.yolo2bbox(box)
            xmin, ymin, xmax, ymax = self.denormalize_ccordinates(w, h, x1, y1, x2, y2)
            self.build_rectangle_opencv(w, image, xmin, ymin, xmax, ymax)
            
        return image

    def plot(self, image_paths, label_paths, num_samples):
        all_images = []
        all_images.extend(glob.glob(image_paths+'/*.jpg'))
        all_images.extend(glob.glob(image_paths+'/*.JPG'))
        all_labels = glob.glob(label_paths)
        
        all_images.sort()
        all_labels.sort()

        num_images = len(all_images)
        
        plt.figure(figsize=(15, 12))
        for i in range(num_samples):
            j = random.randint(0, num_images-1)
            image = cv2.imread(all_images[j])
            with open(all_labels[j], 'r') as f:
                bboxes, labels = [], []
                label_lines = f.readlines()
                for label_line in label_lines:
                    label = label_line[0]
                    bbox_string = label_line[2:]
                    x_c, y_c, w, h = bbox_string.split(' ')
                    x_c, y_c = float(x_c), float(y_c)
                    w, h = float(w), float(h)
                    bboxes.append([x_c, y_c, w, h])
                    labels.append(label)
            result_image = self.plot_box(image, bboxes)
            plt.subplot(2, 2, i+1)
            plt.imshow(result_image[:, :, ::-1])
            plt.axis('off')
        plt.subplots_adjust(wspace=1)
        plt.tight_layout()
        plt.show()


plotter = Plotter()
# Draw example images from dataset
plotter.plot(
    image_paths="pothole_dataset/images/train/",
    label_paths="pothole_dataset/labels/train/*.txt",
    num_samples=4
)